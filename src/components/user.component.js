import React, { Component } from "react";

class UserComponent extends Component {
    constructor() {
        super();
    }

    render() {
        const { user } = this.props;

        return (
            <div>
                {user && user.id > 0 && (
                    <div>
                        <h3>Nom: <span>{user.name}</span></h3>
                        <h3>Occupation: <span>{user.occupation}</span></h3>
                        <h3>Date de naissance: <span>{user.birthdate}</span></h3>
                    </div>
                )}
            </div>
        );
    }
}

export default UserComponent;