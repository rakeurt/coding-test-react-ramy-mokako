import React, { Component } from "react";

class ArticleComponent extends Component {
    constructor() {
        super();
    }

    render() {
        const { articles } = this.props;

        return (
            <div>
                {articles && articles.length > 0 && (
                    <div className='container'>
                        {articles.map((article, index) => {
                            return <div className='card' key={index}>
                                <h2>{article.name}</h2>
                                <p>{article.content}</p>
                            </div>;
                        })}
                    </div>
                )}
            </div>
        );
    }
}

export default ArticleComponent;