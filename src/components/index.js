import UserComponent from './user.component';
import ArticleComponent from './article.component';
export { UserComponent, ArticleComponent}

// Dans les autres fichiers :
// import { Component } from 'components';
