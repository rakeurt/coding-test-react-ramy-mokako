import React, { Component, Fragment } from 'react';
import Helmet from 'react-helmet';
import { Link } from "react-router-dom";
import Icon from '@material-ui/core/Icon';

import { title } from 'utils';


class HomePage extends Component {
    render() {
        return (
            <Fragment>
                <Helmet>
                    { title('Page d\'accueil') }
                </Helmet>

                <div className="home-page content-wrap">

                    <div className="circle x1"></div>
                    <div className="circle x2"></div>

                    <div className="infos-block" style={{ textAlign: 'center', width: 100 + '%', marginTop: 10 + '%' }}>
                        <h1>04h11</h1>
                        <p>Le spécialiste de vos données.</p>
                    </div>

                    <Link to="/users" className="nav-arrow" style={{ right: 10 + 'px', bottom: 3 + 'px'}}>
                        <Icon>arrow_right_alt</Icon>
                    </Link>

                </div>
            </Fragment>
        )
    }
}

export default HomePage;
