import React, { Component, Fragment } from 'react';
import Helmet from 'react-helmet';
import { Link } from "react-router-dom";
import Icon from '@material-ui/core/Icon';

import { title } from 'utils';

import { userService } from 'services';

import { UserComponent, ArticleComponent } from 'components';


class UserPage extends Component {
    constructor(props) {
        super(props);
        this.state = {
            list: [],
            id: 0,
            user: {}
        }
    }

    async componentDidMount() {

        let _id = 0;
        if (this.props.match.params.id) {
            _id = this.props.match.params.id;
        }

        let _users = await userService.list();
        this.setState({ list: _users });
        let _user = _users[0];
        if (_id == 0 && _user != null && _user != undefined) {
            _id = _user.id;
        }
        this.getUserData(_id);
    }

    async getUserData(id) {
        let _user = await userService.get(id);

        if (_user != null && _user != undefined) {
            this.setState({ user: _user, id: _user.id });
            this.props.history.push(window.location.pathname + "users/" + _user.id);
        }
    }

    onChangeUser(event, self) {
        const id = event.target.value;
        self.getUserData(id);
    }

    render() {
        return (
            <Fragment>
                <Helmet>
                    { title('Page secondaire') }
                </Helmet>

                <div className="user-page content-wrap">
                    <Link to="/" className="nav-arrow" style={{ left: 10 + 'px', top: 3 + 'px' }}>
                        <Icon style={{ transform: 'rotate(180deg)' }}>arrow_right_alt</Icon>
                    </Link>

                    <div className="users-select">
                        {this.state.list && this.state.list.length > 0 && (
                            <h1>
                                <select onChange={(e) => { this.onChangeUser(e, this) }} value={this.state.id}>
                                    {this.state.list.map((user, index) => { return <option key={index} value={user.id}>{'Utilisateur # ' + (index + 1)}</option>;})}
                                </select>
                            </h1>
                        )}
                    </div>

                    <div className="infos-block" style={{ textAlign: 'left', width: 50 + '%', margin:'2% auto' }}>
                        <UserComponent user={this.state.user}/>
                    </div>

                    <div className="articles-list" style={{ textAlign: 'left', width: 50 + '%', margin: '2% auto' }}>
                        <ArticleComponent articles={this.state.user.articles} />
                    </div>
                </div>
            </Fragment>
        )
    }
}

export default UserPage;
